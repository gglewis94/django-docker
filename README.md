# django-docker

Ceci est une petite application web faite en django pour tester docker et docker compose.

## Installation
Suivez les étapes que voici pour réussir installation de l'application web.
Ces étapes sont uniquement valables pour les utilisateurs de Linux ou MacOs

### Nécessaire à avoir
Vous devez être sûr d'avoir installé sur votre ordinateur:
* [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [Docker & Docker Compose](https://docs.docker.com/engine/install/)

### Clonez le project
```
git clone git@gitlab.com:pyswirl-devops/django-docker.git
```
ou
```
git clone https://gitlab.com/pyswirl-devops/django-docker.git
```

### Configurez les variables d'environnement: 
Pour le faire :
  * Dupliquez le fichier **.env.sample** et renommez le en **.env**
  * Remplir le nouveau fichier **.env** avec vos propres informations

### Démarrez l'application localement
Lancer l'application avec:
```
cd django-docker
docker compose up -d --build
```

**N'hésitez pas à appliquer les bonnes pratiques en matière de Dockerfile et de docker compose.**

**Happy Coding!**

