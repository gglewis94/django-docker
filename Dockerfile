FROM python:3.8-slim-buster as base

ENV USER="app"

RUN addgroup --gid 1001 --system ${USER} && \
    adduser --no-create-home --shell /bin/false --disabled-password --uid 1001 --system --group ${USER}

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERD 1

WORKDIR /app

COPY requirements.txt .

RUN apt-get update && \
    apt-get install -y --no-install-recommends build-essential && \
    rm -rf /var/lib/apt/lists/* && \
    pip install --upgrade pip setuptools && pip install -r requirements.txt && \
    apt purge -y --auto-remove build-essential

COPY . /app

RUN chown -R ${USER}:${USER} /app && \
    chmod u+x init.sh

USER ${USER}

EXPOSE 8000

ENTRYPOINT ./init.sh